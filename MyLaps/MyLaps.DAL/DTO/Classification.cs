﻿using System.Collections.Generic;
using MyLaps.Model;
using Newtonsoft.Json;

namespace MyLaps.DAL.DTO
{
    public class Classification : IClassification
    {
        public Classification (IEnumerable<Split> splits)
        {
            Splits = splits;
        }
        [JsonProperty("id")]
        public string ClassificationID { get; set; }
        [JsonProperty("rank")]
        public int Rank { get; set; }
        [JsonProperty("bib")]
        public int BibNumber { get; set; }
        [JsonProperty("splits")]
        public IEnumerable<ISplit> Splits { get; set; }
    }
}
