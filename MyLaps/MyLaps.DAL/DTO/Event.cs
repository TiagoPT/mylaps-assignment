﻿using System;
using System.Collections.Generic;
using MyLaps.Model;
using Newtonsoft.Json;

namespace MyLaps.DAL.DTO
{
    public class Event : IEvent
    {
        public Event (IEnumerable<Race> races)
        {
            Races = races;
        }
        [JsonProperty("id")]
        public int EventID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("displayDate")]
        public DateTime Date { get; set; }
        [JsonProperty("appLogoPhotoUrl")]
        public string LogoURL { get; set; }
        [JsonProperty("sponsorPhotoUrl")]
        public string SponsorURL { get; set; }
        [JsonProperty("backgroundPhotoUrl")]
        public string PhotoURL { get; set; }
        [JsonProperty("activity")]
        public string Type { get; set; }
        [JsonProperty("countryCode")]
        public string Country { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("races")]
        public IEnumerable<IRace> Races { get; set; }
    }
}
