﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace MyLaps.DAL.DTO
{
    public class EventDetails
    {
        [JsonProperty("event")]
        public Event Event { get; set; }
        [JsonProperty("races")]
        public IEnumerable<Race> Races { get; set; }

    }
}
