﻿using System.Collections.Generic;
using MyLaps.Model;
using Newtonsoft.Json;

namespace MyLaps.DAL.DTO
{
    public class Race : IRace
    {
        public Race (IEnumerable<Classification> classifications)
        {
            Classifications = classifications;
        }
        [JsonProperty("id")]
        public int RaceID { get; set; }
        [JsonProperty("distanceInMeter")]
        public double Distance { get; set; }
        [JsonProperty("classifications")]
        public IEnumerable<IClassification> Classifications { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
