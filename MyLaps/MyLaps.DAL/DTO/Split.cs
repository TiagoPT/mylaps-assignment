﻿using System;
using MyLaps.Model;
using Newtonsoft.Json;

namespace MyLaps.DAL.DTO
{
    public class Split : ISplit
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("distanceInMeter")]
        public double Distance { get; set; }
        [JsonProperty("speedInKmh")]
        public double Speed { get; set; }
        [JsonProperty("cumulativeTime")]
        public TimeSpan Time { get; set; }
    }
}
