﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using MyLaps.DAL.DTO;
using MyLaps.Model;
using Newtonsoft.Json;

namespace MyLaps.DAL.Providers
{
    public class FileRepository : IEventProvider, IRaceProvider
    {
        private const string EVENTS_SOURCE_PATH_KEY = "MyLaps.DAL.events.json";
        private Dictionary<int, IEvent> Events { get; set; }
        private Dictionary<int, IRace> Races { get; set; }
        public FileRepository ()
        {
            Events = new Dictionary<int, IEvent>();
            Races = new Dictionary<int, IRace>();
            LoadEvents();
        }
        /// <summary>
        ///     Loads the data from embedded json file
        /// </summary>
        private void LoadEvents ()
        {
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("MyLaps.DAL.events.json");
            if(stream == null)
            {
                return;
            }
            try
            {
                string fileText = new StreamReader(stream).ReadToEnd();
                var eventDetails = JsonConvert.DeserializeObject<EventDetails>(fileText);
                if(eventDetails != null)
                {
                    eventDetails.Event.Races = eventDetails.Races;
                    Events.Add(eventDetails.Event.EventID, eventDetails.Event);
                    foreach(var currRace in eventDetails.Races)
                    {
                        Races.Add(currRace.RaceID, currRace);
                    }
                }
            } catch(JsonSerializationException)
            {
                // Some error ocurred while trying to load file from events.json
            }
        }

        public IEnumerable<IEvent> GetEvents ()
        {
            return Events.Values;
        }

        public IEvent GetEvent (int id)
        {
            return Events.ContainsKey(id) ? Events[id] : null;
        }

        public IEnumerable<IRace> GetRaces ()
        {
            return Races.Values;
        }

        public IRace GetRace (int id)
        {
            return Races.ContainsKey(id) ? Races[id] : null;
        }
    }
}
