﻿using System.Collections.Generic;

namespace MyLaps.Model
{
    public interface IClassification
    {
        string ClassificationID { get; set; }
        int Rank { get; set; }
        int BibNumber { get; set; }
        IEnumerable<ISplit> Splits { get; set; }
    }
}
