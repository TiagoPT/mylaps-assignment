﻿using System;
using System.Collections.Generic;

namespace MyLaps.Model
{
    public interface IEvent
    {
        int EventID { get; set; }
        string Name { get; set; }
        DateTime Date { get; set; }
        string LogoURL { get; set; }
        string SponsorURL { get; set; }
        string PhotoURL { get; set; }
        string Type { get; set; }
        string Country { get; set; }
        string City { get; set; }
        IEnumerable<IRace> Races { get; set; }
    }
}
