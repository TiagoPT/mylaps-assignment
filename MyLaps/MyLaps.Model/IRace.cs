﻿using System.Collections.Generic;

namespace MyLaps.Model
{
    public interface IRace
    {
        int RaceID { get; set; }
        string Name { get; set; }
        double Distance { get; set; }
        IEnumerable<IClassification> Classifications { get; set; }
    }
}
