﻿using System;

namespace MyLaps.Model
{
    public interface ISplit
    {
        string Name { get; set; }
        double Distance { get; set; }
        double Speed { get; set; }
        TimeSpan Time { get; set; }
    }
}
