﻿using System.Collections.Generic;

namespace MyLaps.Model
{
    public interface IEventProvider
    {
        IEnumerable<IEvent> GetEvents ();
        IEvent GetEvent (int id);
    }
}
