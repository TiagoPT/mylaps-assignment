﻿using System.Collections.Generic;

namespace MyLaps.Model
{
    public interface IRaceProvider
    {
        IEnumerable<IRace> GetRaces ();
        IRace GetRace (int id);
    }
}
