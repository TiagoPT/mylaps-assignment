﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using MyLaps.DAL.Providers;
using MyLaps.Model;

namespace MyLaps.Web.App_Start
{
    public class DIConfig
    {
        public static void Config ()
        {

            var builder = new ContainerBuilder();

            // You can register controllers all at once using assembly scanning...
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<FileRepository>()
                .As<IEventProvider>()
                .As<IRaceProvider>()
                .SingleInstance();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }
    }
}
