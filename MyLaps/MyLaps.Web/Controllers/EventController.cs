﻿using System.Linq;
using System.Web.Mvc;
using MyLaps.Model;

namespace MyLaps.Web.Controllers
{
    public class EventController : Controller
    {
        public IEventProvider EventProvider { get; set; }

        public EventController (IEventProvider eventProvider)
        {
            EventProvider = eventProvider;
        }

        // GET: Event
        public ActionResult Index ()
        {
            return View();
        }
        public ActionResult Details (int id)
        {
            IEvent evt = EventProvider.GetEvent(id);
            if(evt == null)
            {
                return RedirectToAction("Index");
            }
            return View(evt);
        }
        [ChildActionOnly]
        public PartialViewResult LatestEvents ()
        {
            var lastEvents = EventProvider.GetEvents()
                                .OrderByDescending(e => e.Date)
                                .Take(5);
            return PartialView("_LatestEvents", lastEvents);
        }

    }
}