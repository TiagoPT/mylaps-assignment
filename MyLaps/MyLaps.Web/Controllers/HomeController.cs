﻿using System.Web.Mvc;

namespace MyLaps.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index ()
        {
            return View();
        }
        public ActionResult About ()
        {
            return View();
        }
    }
}