﻿using System.Web.Mvc;
using MyLaps.Model;

namespace MyLaps.Web.Controllers
{
    public class RaceController : Controller
    {
        IRaceProvider RaceProvider { get; set; }
        public RaceController (IRaceProvider raceProvider)
        {
            RaceProvider = raceProvider;
        }
        // GET: Race
        public ActionResult Details (int id)
        {
            IRace race = RaceProvider.GetRace(id);
            if(race == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(race);
        }
    }
}