﻿using System.Web.Mvc;
using System.Web.Routing;
using MyLaps.Web.App_Start;

namespace MyLaps.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start ()
        {
            DIConfig.Config();

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
