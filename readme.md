# Assignment for Web Developer Candidates at MyLaps  

[ ![MyLaps Company Logo](https://www.mylaps.com/images/mylaps-logo.svg) ](https://www.mylaps.com/en)  

This repository contains the code developed for the [MyLaps](https://www.mylaps.com) assignment.

This assignment was made by [Tiago Martins](mailto:tiagofcmartins@gmail.com) for the job application at the 3rd of December of 2015.

## Description

For a local racing event we would like you to create a website for the race results.  
The assignment is not meant to be difficult or to take too much time. We only want to see your approach to the problem, and how you think about website design and usability.  
   
The race results can be found in the attached JSON file. The JSON has one event, containing 2 races, and each race contains 5 athlete classifications with their split times.  

The users of the website should at least be able to:   
* Select a race within the event.  
* Show the athlete classifications for a race.  
* Show the splits for a classification.  


We would like you to use Visual Studio to build the website, but the choice of language and/or frameworks is up to you.  

After you complete the assignment, please put all files that you consider relevant in a zip archive and send it to us.  
You will be supplied with an email address where you can send the archive to We hope this assignment will be fun!